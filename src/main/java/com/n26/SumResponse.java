package com.n26;

/**
 * @author Igor
 */
public class SumResponse {
    private double sum;

    public SumResponse() {
    }

    public SumResponse(double sum) {
        this.sum = sum;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }
}
