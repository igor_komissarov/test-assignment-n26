package com.n26;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author Igor
 */
@RestController
@RequestMapping("/transactionservice")
public class TransactionService {

    final Map<Long, Transaction> store = new ConcurrentHashMap<>();
    final Map<String, Set<Long>> typeStore = new ConcurrentHashMap<>();
    final Map<Long, Set<Long>> sumStore = new ConcurrentHashMap<>();

    @RequestMapping(value = "/transaction/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CreateResponse> create(@RequestBody Transaction transaction, @PathVariable long id) {
        if(store.putIfAbsent(id, transaction) != null) {
            return new ResponseEntity<>(new CreateResponse("Failed: Duplicate id"), HttpStatus.BAD_REQUEST);
        }

        Set<Long> list = new CopyOnWriteArraySet<>();
        Set<Long> oldList = typeStore.putIfAbsent(transaction.getType(), list);
        if(oldList != null) {
            oldList.add(id);
        } else {
            list.add(id);
        }

        if(transaction.getParent_id() != null) {
            Set<Long> set = new CopyOnWriteArraySet<>();
            Set<Long> oldSet = sumStore.putIfAbsent(transaction.getParent_id(), set);
            if(oldSet != null) {
                oldSet.add(id);
            } else {
                set.add(id);
            }
        }

        return new ResponseEntity<>(new CreateResponse("OK"), HttpStatus.OK);
    }

    @RequestMapping(value = "/transaction/{id}", method = RequestMethod.GET)
    public Transaction findById(@PathVariable long id) {
        return store.get(id);
    }

    @RequestMapping(value = "/types/{type}", method = RequestMethod.GET)
    public Set<Long> findByType(@PathVariable String type) {
        return typeStore.get(type);
    }

    @RequestMapping(value = "/sum/{id}", method = RequestMethod.GET)
    public SumResponse sum(@PathVariable long id) {
        Transaction transaction = store.get(id);
        if(transaction == null) return new SumResponse();
        double sum = transaction.getAmount() + sumOfChildren(sumStore.get(id));
        return new SumResponse(sum);
    }

    private double sumOfChildren(Set<Long> ids) {
        if(ids == null) return 0.0;
        double sum = 0.0;
        for(Long id : ids) {
            sum += store.get(id).getAmount();
            sum += sumOfChildren(sumStore.get(id));
        }
        return sum;
    }
}
