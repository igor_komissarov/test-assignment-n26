package com.n26;

import java.util.Objects;

/**
 * @author Igor
 */
public class Transaction {
    private double amount;
    private String type;
    private Long parent_id;

    public Transaction() {
    }

    public Transaction(double amount, String type, Long parent_id) {
        this.amount = amount;
        this.type = type;
        this.parent_id = parent_id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Double.compare(that.amount, amount) == 0 &&
                Objects.equals(type, that.type) &&
                Objects.equals(parent_id, that.parent_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, type, parent_id);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "amount=" + amount +
                ", type='" + type + '\'' +
                ", parent_id=" + parent_id +
                '}';
    }
}
