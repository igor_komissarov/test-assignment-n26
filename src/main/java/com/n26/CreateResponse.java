package com.n26;

/**
 * @author Igor
 */
public class CreateResponse {
    private String status;

    public CreateResponse() {
    }

    public CreateResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
