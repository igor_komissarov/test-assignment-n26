package com.n26;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * @author Igor
 */
public class TransactionServiceTest {
    @Test
    public void testCommon() throws Exception {
        TransactionService service = new TransactionService();

        Transaction transaction1 = new Transaction(10.0, "cars", null);
        service.create(transaction1, 1);
        assertEquals(transaction1, service.findById(1));

        Transaction transaction2 = new Transaction(20.0, "cars", 1L);
        service.create(transaction2, 2);
        assertArrayEquals(new Long[]{1L, 2L}, service.findByType("cars").toArray(new Long[0]));
        assertEquals(20.0, service.sum(2).getSum(), 0.01);
        assertEquals(20.0 + 10.0, service.sum(1).getSum(), 0.01);

        Transaction transactionDupe = new Transaction(30.0, "cars", null);
        service.create(transactionDupe, 1);
        assertEquals(transaction1, service.findById(1));
    }

    @Test
    public void testChildrenBeforeParent() throws Exception {
        TransactionService service = new TransactionService();

        Transaction transaction1 = new Transaction(10.0, "cars", null);
        Transaction transaction2 = new Transaction(20.0, "cars", 1L);
        Transaction transaction3 = new Transaction(30.0, "cars", 2L);
        service.create(transaction1, 1);
        service.create(transaction3, 3);
        service.create(transaction2, 2);

        assertEquals(30.0, service.sum(3).getSum(), 0.01);
        assertEquals(50.0, service.sum(2).getSum(), 0.01);
        assertEquals(60.0, service.sum(1).getSum(), 0.01);
    }
}