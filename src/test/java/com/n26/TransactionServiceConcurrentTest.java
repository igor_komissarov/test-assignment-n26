package com.n26;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.*;

/**
 * @author ikomissarov
 */
public class TransactionServiceConcurrentTest {

    public static final int THREADS = 5;
    public static final int TRANSACTIONS = 100;
    public static final int IDS = THREADS * TRANSACTIONS * 10;
    public static final String[] types = new String[]{"x-type", "y-type", "z-type", "a-type", "b-type"};

    private static final Logger log = LoggerFactory.getLogger("Test");

    private final TransactionService service = new TransactionService();

    Map<Long, Transaction> store = new ConcurrentHashMap<>();
    Map<String, Set<Long>> typeStore = new ConcurrentHashMap<>();
    Map<Long, Double> sumStore = new ConcurrentHashMap<>();

    @Test
    public void concurrentTest() throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(THREADS);
        CyclicBarrier barrier = new CyclicBarrier(THREADS);

        for(int i = 0; i < THREADS; i++) {
            executorService.execute(() -> {
                try { //wait until all threads are ready to run
                    barrier.await();
                } catch(InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                    return;
                }
                Random randomId = new Random();
                Random randomParentId = new Random();
                Random randomType = new Random();
                for(int j = 0; j < TRANSACTIONS; j++) {
                    int id = randomId.nextInt(IDS);
                    int parentId = randomParentId.nextInt(IDS);
                    String type = types[randomType.nextInt(types.length)];
                    Transaction transaction = new Transaction(100 * id, type, (parentId < id ? (long) parentId : null));
                    ResponseEntity<CreateResponse> response = service.create(transaction, id);
                    log.info("{}, id={}, code={}", transaction, id, response.getStatusCode());
                    if(response.getStatusCode() == HttpStatus.OK) store.put((long) id, transaction);
                }
            });
        }

        executorService.shutdown();
        executorService.awaitTermination(1L, TimeUnit.HOURS);

        calculateAndCompareResults();
    }

    private void calculateAndCompareResults() {
        for(Long id : store.keySet()) {
            Transaction transaction = store.get(id);

            Set<Long> list = new CopyOnWriteArraySet<>();
            Set<Long> oldList = typeStore.putIfAbsent(transaction.getType(), list);
            if(oldList != null) {
                oldList.add(id);
            } else {
                list.add(id);
            }

            double amount = transaction.getAmount();
            Double sum = sumStore.putIfAbsent(id, amount);
            log.debug("{}: adding {} to {}", id, amount, sum);
            if(sum != null) {
                sumStore.put(id, sum + amount);
            }

            //update sum for all the parent transactions
            //handle the case when child transaction comes before parent
            while(transaction != null && transaction.getParent_id() != null) {
                Double parentSum = sumStore.putIfAbsent(transaction.getParent_id(), amount);
                log.debug("{}: adding {} to {}", transaction.getParent_id(), amount, parentSum);
                if(parentSum != null) {
                    sumStore.put(transaction.getParent_id(), parentSum + amount);
                }
                transaction = store.get(transaction.getParent_id());
            }
        }

        //Assert.assertEquals(sumStore.keySet(), service.sumStore.keySet());
        /*TreeMap<Long, Double> sortedMap = new TreeMap<>(sumStore);
        for(Long id : sortedMap.keySet()) {
            log.info("{} - {} - {}", id, sumStore.get(id), service.sumStore.get(id));
        }*/

        Assert.assertEquals(store, service.store);
        Assert.assertEquals(typeStore, service.typeStore);
        sumStore.keySet().stream().filter(id -> service.findById(id) != null)
                .forEach(id -> Assert.assertEquals(sumStore.get(id), service.sum(id).getSum(), 0.01));
    }
}
